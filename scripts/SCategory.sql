INSERT INTO samoim.SCategory (NAME,PHOTO_PATH) VALUES
	 ('클라이밍','climbing'),
	 ('산책','golf'),
	 ('스쿠버다이빙','tennis'),
	 ('드라이브','drive'),
	 ('캠핑','camping'),
	 ('콘서트','concert'),
	 ('뮤지컬','musical'),
	 ('전시','exhibition'),
	 ('밴드','band'),
	 ('작곡','composition');
INSERT INTO samoim.SCategory (NAME,PHOTO_PATH) VALUES
	 ('드로잉','drawing'),
	 ('글쓰기','writing'),
	 ('독서','reading'),
	 ('스터디','study'),
	 ('외국어','foreign'),
	 ('유기견봉사','dog'),
	 ('재능기부','donation'),
	 ('요리','cooking'),
	 ('디저트','dessert'),
	 ('전통주','soju');
